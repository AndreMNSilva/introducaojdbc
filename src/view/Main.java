/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import dao.ClienteDAO;
import dao.DataSource;
import java.util.ArrayList;
import java.util.Scanner;
import model.Cliente;


/**
 *
 * @author internet
 */
public class Main {
    DataSource dataSource = new DataSource();
    ClienteDAO dao = new ClienteDAO(dataSource);
    Cliente cliente = new Cliente();
    Scanner sc = new Scanner(System.in);
    
    
    public static void main(String[] args) {
        Main m = new Main();
        m.inicio();
    }
    
    public void inicio(){
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();

        System.out.printf("(1) Buscar Clientes\n");
        System.out.printf("(2) Inserir Clientes\n");
        System.out.printf("(3) Deletar Clientes\n");
        
        int opcao = sc.nextInt();
        
        exec(opcao, clientes);
    }
    
    public void exec(int opcao, ArrayList<Cliente> clientes){
        switch(opcao){
        
            case 1:
                clientes = dao.buscarTudo();
                if(clientes.size() > 0){
                    System.out.println("\nResultado\n");
                    for(Cliente c : clientes){
                        System.out.println("Nome:" + c.getNome());
                        System.out.println("Email:" + c.getEmail());
                        System.out.println("Telefone:" + c.getTelefone());
                    }
                }else{
                    System.out.println("\nNenhum resultado encontrado\n");
                }
                inicio();
            case 2:
                System.out.printf("Nome:\n");
                cliente.setNome(sc.next());
                
                System.out.printf("Email:\n");
                cliente.setEmail(sc.next());
                
                System.out.printf("Telefone:\n");
                cliente.setTelefone(sc.next());
                
                boolean retorno = dao.inserir(cliente);
                if(retorno){
                    System.out.println("\nInserido com sucesso!");
                }
                inicio();
            case 3:
                boolean ret = dao.deletar();
                if(ret){
                    System.out.println("\nDeletado com sucesso!");
                }
                inicio();
        }
    }
}
