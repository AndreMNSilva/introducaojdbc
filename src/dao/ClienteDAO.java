package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Cliente;

public class ClienteDAO{
    private DataSource dataSource;
    
    public ClienteDAO(DataSource dataSource){
        this.dataSource = dataSource;
    }
    
    public ArrayList<Cliente> buscarTudo(){
        try{
            String SQL = "SELECT * from Cliente";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while(rs.next()){
                Cliente cli = new Cliente();
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
            return lista;
            
        }
        catch (SQLException ex){
            System.err.println("Erro ao recuperar "+ex.getMessage());
        }
        catch (Exception ex){
            System.err.println("Erro geral "+ex.getMessage());
        }
        return null;
    }
    
    public boolean inserir(Cliente c){
         boolean retorno = false;
        try{
           
            String SQL = "INSERT INTO Cliente"
		+ "(nome, email, telefone) VALUES"
		+ "(?,?,?)";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL); 
            ps.setString(1, c.getNome());
            ps.setString(2, c.getEmail());
            ps.setString(3, c.getTelefone());
            ps.executeUpdate();
          
            retorno = true;
        }
        catch (SQLException ex){
            System.err.println("Erro ao recuperar "+ex.getMessage());
        }
        catch (Exception ex){
            System.err.println("Erro geral "+ex.getMessage());
        }
        return retorno;
    }
    
    public boolean deletar(){
        boolean retorno = false;
        try{
            String SQL = "DELETE FROM Cliente LIMIT 1;";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ps.executeUpdate();
          
            retorno = true;
        }
        catch (SQLException ex){
            System.err.println("Erro ao recuperar "+ex.getMessage());
        }
        catch (Exception ex){
            System.err.println("Erro geral "+ex.getMessage());
        }
        return retorno;
    }
}